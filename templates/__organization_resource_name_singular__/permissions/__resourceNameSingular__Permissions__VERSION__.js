/* eslint-disable max-len */
// const RolesEnum = require('../../../app_core/components/roles/enums/RolesEnum');
const BasePermissions = require('web_core/permissions/BasePermissions');
const accessControlUtils = require('web_core/utils/accessControlUtils');
const BasicControllerMethodsEnum = require('web_core/enums/BasicControllerMethodsEnum');

const CREATE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE = 'CREATE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE';
const CREATE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE = 'CREATE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE';

const GET_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE = 'GET_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE';
const GET_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE = 'GET_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE';

const UPDATE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE = 'UPDATE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE';
const UPDATE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE = 'UPDATE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE';

const DELETE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE = 'DELETE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE';
const DELETE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE = 'DELETE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE';

const __RESOURCE_NAME_PLURAL___MASTER_ROLE = '__RESOURCE_NAME_PLURAL___MASTER_ROLE';

/**
 * Classe responsável por definir as permissões de acesso das rotas de Organization.
 *
 * @extends BasePermissions
 */
class OrganizationPermissions extends BasePermissions {
  /**
   * @constructor
   */
  constructor() {
    super();
    this.RESOURCE_NAME = '__RESOURCE_NAME_SINGULAR__';
  }

  /**
   * @override
   */
  getGrants() {
    return [
      // Create
      {
        role: CREATE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.CREATE,
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requesterOrganizationId: '$.requestedOrganizationId',
          },
        },
      },
      {
        role: CREATE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.CREATE,
        attributes: ['*'],
      },
      // Get & List
      {
        role: GET_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.GET, BasicControllerMethodsEnum.LIST],
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requesterOrganizationId: '$.requestedOrganizationId',
          },
        },
      },
      {
        role: GET_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.GET, BasicControllerMethodsEnum.LIST],
        attributes: ['*'],
      },
      // Update
      {
        role: UPDATE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.UPDATE,
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requesterOrganizationId: '$.requestedOrganizationId',
          },
        },
      },
      {
        role: UPDATE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.UPDATE,
        attributes: ['*'],
      },
      // Delete
      {
        role: DELETE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.DELETE,
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requesterOrganizationId: '$.requestedOrganizationId',
          },
        },
      },
      {
        role: DELETE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.DELETE],
        attributes: ['*'],
      },
      // Master
      {
        role: __RESOURCE_NAME_PLURAL___MASTER_ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.CREATE, BasicControllerMethodsEnum.GET, BasicControllerMethodsEnum.LIST, BasicControllerMethodsEnum.UPDATE, BasicControllerMethodsEnum.DELETE],
        attributes: ['*'],
      }];
  }

  /**
   * @override
   */
  getExtends() {
    return [];
    // return [
    //   {
    //     role: RolesEnum.ENEXT_ROOT_ROLE,
    //     childrenRoles: [
    //       __RESOURCE_NAME_PLURAL___MASTER_ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.ENEXT_ADMIN_ROLE,
    //     childrenRoles: [
    //       CREATE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
    //       UPDATE_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
    //       GET_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.ENEXT_STAFF_ROLE,
    //     childrenRoles: [
    //       GET_ANY___RESOURCE_NAME_SINGULAR___OF_ANY_ORGANIZATION_ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.EXTERNAL_ORGANIZATION_ADMIN_ROLE,
    //     childrenRoles: [
    //       CREATE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    //       GET_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    //       UPDATE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    //       DELETE_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.EXTERNAL_ORGANIZATION_STAFF_ROLE,
    //     childrenRoles: [
    //       GET_ANY___RESOURCE_NAME_SINGULAR___OF_MY_ORGANIZATION_ROLE,
    //     ],
    //   },
    // ];
  }
}

const resourcePermissions = new OrganizationPermissions();

resourcePermissions.init(accessControlUtils);

module.exports = resourcePermissions;
