const mongoose = require('mongoose');
const modelUtils = require('web_core/utils/modelUtils');
require('mongoose-schema-jsonschema')(mongoose);
const semanticId = require('web_core/schema/types/semanticIdSchamaType');

// Definição do esquema
// eslint-disable-next-line camelcase
const __ResourceNameSingular__Schema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    auto: true,
    description: 'Id do recurso.',
  },
  semanticId,
  organizationId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organization',
    required: true,
    description: 'Id do organizationId dono desse recurso.',
  },
  name: {
    type: String,
    required: false,
    maxlength: 128,
    description: 'Nome do recurso.',
  },
  description: {
    type: String,
    required: false,
    maxlength: 1024,
    description: 'Descrição do recurso.',
  },
}, {
  timestamps: true,
});

// Índices
__ResourceNameSingular__Schema.index({createdAt: 1});
__ResourceNameSingular__Schema.index({createdAt: -1});
__ResourceNameSingular__Schema.index({updatedAt: 1});
__ResourceNameSingular__Schema.index({updatedAt: -1});
__ResourceNameSingular__Schema.index({organizationId: 1, semanticId: 1}, {unique: true});

// Atributos protegidos no update
const updateProtectedAttributes = [
  'semanticId',
  'organizationId',
  'createdAt',
  'updatedAt',
];

// Atributos que podem ser utilizados no sort
const sortableAttributes = [
  'createdAt',
  'updatedAt',
];

module.exports = modelUtils.buildFromSchema({
  Schema: __ResourceNameSingular__Schema,
  modelName: '__ResourceNameSingular__',
  updateProtectedAttributes,
  sortableAttributes,
});
