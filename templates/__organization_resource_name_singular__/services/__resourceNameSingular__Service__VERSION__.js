/* eslint-disable camelcase */
const __ResourceNameSingular__Model = require('../models/__ResourceNameSingular__Model');
const BaseOrganizationResourceCrudService = require('web_core/services/BaseOrganizationResourceCrudService');
// const log = require('web_core/utils/logUtils');
// const nconf = require('nconf');

/**
 * Classe de serviço relacionada ao recurso __ResourceNameSingular__.
 * @extends BaseCrudService
 */
class __ResourceNameSingular__ServiceV1 extends BaseOrganizationResourceCrudService {}

const serviceInstance = new __ResourceNameSingular__ServiceV1({
  Model: __ResourceNameSingular__Model,
});

module.exports = serviceInstance;
