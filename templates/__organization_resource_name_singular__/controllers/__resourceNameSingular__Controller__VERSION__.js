/* eslint-disable camelcase */
const ResourceBaseController = require('web_core/controllers/ResourceBaseController');
const resourceService = require('../services/__resourceNameSingular__Service__VERSION__');
const organizationService = require('../../organization/services/organizationServiceV1'); // TODO: Generalizar o caminho, versão ou nome de organization
const resourcePermissions = require('../permissions/__resourceNameSingular__Permissions__VERSION__');
const BasicControllerMethodsEnum = require('web_core/enums/BasicControllerMethodsEnum');
const nconf = require('nconf');
const {param} = require('express-validator');

const checkPaginationQueryParametersMiddleware = require('web_core/middlewares/checkPaginationQueryParametersMiddleware');
const checkPermissionMiddleware = require('web_core/middlewares/checkPermissionsMiddleware');
const checkResourceMiddleware = require('web_core/middlewares/checkResourceMiddleware');
const checkSemanticIdMiddleware = require('web_core/middlewares/checkSemanticIdMiddleware');
const handleBadRequestErrorsMiddleware = require('web_core/middlewares/handleBadRequestErrorsMiddleware');
const injectDefaultQueryParamatersMiddleware = require('web_core/middlewares/injectDefaultQueryParamatersMiddleware');
const injectInBodyMiddleware = require('web_core/middlewares/injectInBodyMiddleware');
const injectQueryParametersMiddleware = require('web_core/middlewares/injectQueryParametersMiddleware');

/**
 * Classe de controller relacionada ao recurso __ResourceNameSingular__.
 *
 * @extends ResourceBaseController
 */
class __ResourceNameSingular__Controller__VERSION__ extends ResourceBaseController {
  /**
   * @constructor
   * @param {Object} params
   * @param {resourceService} params.resourceService
   * @param {resourcePermissions} params.resourcePermissions
   */
  constructor({
    resourceService,
    organizationService,
    resourcePermissions,
  }) {
    super({
      resourceService,
      resourcePermissions,
    });

    this.organizationService = organizationService;
    this.permissionContext = () => (req) => {
      return {
        requesterOrganizationId: req.user.organizationId,
        requestedOrganizationId: req.params.organizationId,
      };
    };
  }

  /**
   * @override
   */
  createPreMiddlewares() {
    return [
      checkSemanticIdMiddleware([
        {
          paramKey: 'organizationId',
          mapToResource: async (req) => {
            return await this.organizationService.findOne({
              q: {
                semanticId: req.params.organizationId,
              },
            });
          },
        },
      ]),
      checkPermissionMiddleware({
        execute: BasicControllerMethodsEnum.CREATE,
        on: this.resourcePermissions.RESOURCE_NAME,
        context: this.permissionContext(),
      }),
      handleBadRequestErrorsMiddleware(),
      injectInBodyMiddleware((req) => {
        return {
          organizationId: req.params.organizationId,
        };
      }),
    ];
  }

  /**
   * @override
   */
  getPreMiddlewares() {
    return [
      checkSemanticIdMiddleware([
        {
          paramKey: 'organizationId',
          mapToResource: async (req) => {
            return await this.organizationService.findOne({
              q: {
                semanticId: req.params.organizationId,
              },
            });
          },
        },
        {
          paramKey: '_id',
          mapToResource: async (req) => {
            return await this.resourceService.findOne({
              q: {
                semanticId: req.params._id,
                organizationId: req.params.organizationId,
              },
            });
          },
        },
      ]),
      checkPermissionMiddleware({
        execute: BasicControllerMethodsEnum.GET,
        on: resourcePermissions.RESOURCE_NAME,
        context: this.permissionContext(),
      }),
      param('organizationId')
        .isMongoId('The path param \'organizationId\ must be a valid mongo id if you aren\'t using semanticId.'),
      param('_id')
        .isMongoId('The path param \'_id\ must be a valid mongo id if you aren\'t using semanticId.'),
      handleBadRequestErrorsMiddleware(),
      checkResourceMiddleware(async (req) => {
        return await resourceService.findOne({
          q: {
            _id: req.params._id,
            organizationId: req.params.organizationId,
          },
          populate: req.query.populate,
        });
      }),
    ];
  }

  /**
   * @override
   */
  listPreMiddlewares() {
    return [
      checkSemanticIdMiddleware([
        {
          paramKey: 'organizationId',
          mapToResource: async (req) => {
            return await this.organizationService.findOne({
              q: {
                semanticId: req.params.organizationId,
              },
            });
          },
        },
      ]),
      checkPermissionMiddleware({
        execute: BasicControllerMethodsEnum.GET,
        on: this.resourcePermissions.RESOURCE_NAME,
        context: this.permissionContext(),
      }),
      injectDefaultQueryParamatersMiddleware(nconf.get('defaultListQueryParameters')),
      injectQueryParametersMiddleware((req) => {
        if (req.params.organizationId) {
          return {
            organizationId: req.params.organizationId,
          };
        } else {
          return {};
        }
      }),
      checkPaginationQueryParametersMiddleware(),
      handleBadRequestErrorsMiddleware(),
    ];
  }

  /**
   * @override
   */
  updatePreMiddlewares() {
    return [
      checkSemanticIdMiddleware([
        {
          paramKey: 'organizationId',
          mapToResource: async (req) => {
            return await this.organizationService.findOne({
              q: {
                semanticId: req.params.organizationId,
              },
            });
          },
        },
        {
          paramKey: '_id',
          mapToResource: async (req) => {
            return await this.resourceService.findOne({
              q: {
                semanticId: req.params._id,
                organizationId: req.params.organizationId,
              },
            });
          },
        },
      ]),
      checkPermissionMiddleware({
        execute: BasicControllerMethodsEnum.UPDATE,
        on: this.resourcePermissions.RESOURCE_NAME,
        context: this.permissionContext(),
      }),
      param('organizationId')
        .isMongoId('The path param \'organizationId\ must be a valid mongo id if you aren\'t using semanticId.'),
      param('_id')
        .isMongoId('The path param \'_id\ must be a valid mongo id if you aren\'t using semanticId.'),
      handleBadRequestErrorsMiddleware(),
      checkResourceMiddleware(async (req) => {
        return await this.resourceService.findOne({
          q: {
            _id: req.params._id,
            organizationId: req.params.organizationId,
          }, populate: req.query.populate,
        });
      }),
      handleBadRequestErrorsMiddleware(),
    ];
  }

  /**
   * @override
   */
  deletePreMiddlewares() {
    return [
      checkSemanticIdMiddleware([
        {
          paramKey: 'organizationId',
          mapToResource: async (req) => {
            return await this.organizationService.findOne({
              q: {
                semanticId: req.params.organizationId,
              },
            });
          },
        },
        {
          paramKey: '_id',
          mapToResource: async (req) => {
            return await this.resourceService.findOne({
              q: {
                semanticId: req.params._id,
                organizationId: req.params.organizationId,
              },
            });
          },
        },
      ]),
      checkPermissionMiddleware({
        execute: BasicControllerMethodsEnum.DELETE,
        on: this.resourcePermissions.RESOURCE_NAME,
        context: this.permissionContext(),
      }),
      checkResourceMiddleware(async (req) => {
        return await this.resourceService.findOne({
          q: {
            _id: req.params._id,
            organizationId: req.params.organizationId,
          }, populate: req.query.populate,
        });
      }),
      handleBadRequestErrorsMiddleware(),
    ];
  }

  /**
   * @override
   */
  init() {
    super.init();
  }
}

const controllerInstance = new __ResourceNameSingular__Controller__VERSION__({
  resourceService,
  organizationService,
  resourcePermissions,
});

controllerInstance.init();

module.exports = controllerInstance;
