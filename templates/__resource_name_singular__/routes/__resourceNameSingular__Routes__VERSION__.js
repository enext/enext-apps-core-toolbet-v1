const express = require('express');
const router = new express.Router();
const BasicControllerMethodsEnum = require('web_core/enums/BasicControllerMethodsEnum');
// eslint-disable-next-line camelcase
const __resourceNameSingular__Controller__VERSION__ = require('../controllers/__resourceNameSingular__Controller__VERSION__');

/**
 * @swagger
 *
 * tags:
 *   - name: __ResourceNameSingular__
 *     description: ''
 */

/**
 * @swagger
 *
 * /api/__version__/__resource-name-plural__:
 *   post:
 *     tags:
 *       - __ResourceNameSingular__
 *     description: Cria um novo recurso.
 *     requestBody:
 *       content:
 *          'application/json':
 *             schema:
 *               $ref: '#/components/schemas/__ResourceNameSingular___createRequestSchema'
 *     responses:
 *       200:
 *         description: Criado com sucesso.
 *         content:
 *           'application/json':
 *             schema:
 *               $ref: '#/components/schemas/__ResourceNameSingular___getResponseSchema'
 *       400:
 *         $ref: '#/components/responses/400_ErrorResponse'
 *       401:
 *         $ref: '#/components/responses/401_ErrorResponse'
 *       403:
 *         $ref: '#/components/responses/403_ErrorResponse'
 *       500:
 *         $ref: '#/components/responses/500_ErrorResponse'
 */
router.post(
  '/api/__version__/__resource-name-plural__',
  ...__resourceNameSingular__Controller__VERSION__.getMiddlewares(BasicControllerMethodsEnum.CREATE),
);

/**
 * @swagger
 *
 * /api/__version__/__resource-name-plural__/{_id}:
 *   get:
 *     tags:
 *       - __ResourceNameSingular__
 *     description: Obtém um novo recurso.
 *     parameters:
 *       - $ref: '#/components/parameters/_id'
 *       - $ref: '#/components/parameters/useSemanticId'
 *     responses:
 *       200:
 *         description: Recurso obtido com sucesso.
 *         content:
 *           'application/json':
 *             schema:
 *               $ref: '#/components/schemas/__ResourceNameSingular___getResponseSchema'
 *       400:
 *         $ref: '#/components/responses/400_ErrorResponse'
 *       401:
 *         $ref: '#/components/responses/401_ErrorResponse'
 *       403:
 *         $ref: '#/components/responses/403_ErrorResponse'
 *       404:
 *         $ref: '#/components/responses/404_ErrorResponse'
 *       500:
 *         $ref: '#/components/responses/500_ErrorResponse'
 */
router.get(
  '/api/__version__/__resource-name-plural__/:_id',
  ...__resourceNameSingular__Controller__VERSION__.getMiddlewares(BasicControllerMethodsEnum.GET),
);

/**
 * @swagger
 *
 * /api/__version__/__resource-name-plural__:
 *   get:
 *     tags:
 *       - __ResourceNameSingular__
 *     description: Lista os recursos. Retorno paginado.
 *     parameters:
 *       - $ref: '#/components/parameters/limit'
 *       - $ref: '#/components/parameters/page'
 *     responses:
 *       200:
 *         description: Página com todos os recursos obtidos.
 *         content:
 *           'application/json':
 *             schema:
 *               $ref: '#/components/schemas/__ResourceNameSingular___listResponseSchema'
 *       400:
 *         $ref: '#/components/responses/400_ErrorResponse'
 *       401:
 *         $ref: '#/components/responses/401_ErrorResponse'
 *       403:
 *         $ref: '#/components/responses/403_ErrorResponse'
 *       500:
 *         $ref: '#/components/responses/500_ErrorResponse'
 */
router.get(
  '/api/__version__/__resource-name-plural__',
  ...__resourceNameSingular__Controller__VERSION__.getMiddlewares(BasicControllerMethodsEnum.LIST),
);

/**
 * @swagger
 *
 * /api/__version__/__resource-name-plural__/{_id}:
 *   patch:
 *     tags:
 *       - __ResourceNameSingular__
 *     description: Atualiza um recurso.
 *     parameters:
 *       - $ref: '#/components/parameters/_id'
 *       - $ref: '#/components/parameters/useSemanticId'
 *     requestBody:
 *       content:
 *          'application/json':
 *             schema:
 *               $ref: '#/components/schemas/__ResourceNameSingular___updateRequestSchema'
 *     responses:
 *       200:
 *         description: Atualizado com sucesso.
 *         content:
 *           'application/json':
 *             schema:
 *               $ref: '#/components/schemas/__ResourceNameSingular___getResponseSchema'
 *       400:
 *         $ref: '#/components/responses/400_ErrorResponse'
 *       401:
 *         $ref: '#/components/responses/401_ErrorResponse'
 *       403:
 *         $ref: '#/components/responses/403_ErrorResponse'
 *       404:
 *         $ref: '#/components/responses/404_ErrorResponse'
 *       500:
 *         $ref: '#/components/responses/500_ErrorResponse'
 */
router.patch(
  '/api/__version__/__resource-name-plural__/:_id',
  ...__resourceNameSingular__Controller__VERSION__.getMiddlewares(BasicControllerMethodsEnum.UPDATE),
);

/**
 * @swagger
 *
 * /api/__version__/__resource-name-plural__/{_id}:
 *   delete:
 *     tags:
 *       - __ResourceNameSingular__
 *     description: Deleta o recurso.
 *     parameters:
 *       - $ref: '#/components/parameters/_id'
 *       - $ref: '#/components/parameters/useSemanticId'
 *     responses:
 *       204:
 *         description: Recurso removido com sucesso. Sem conteúdo.
 *       400:
 *         $ref: '#/components/responses/400_ErrorResponse'
 *       401:
 *         $ref: '#/components/responses/401_ErrorResponse'
 *       403:
 *         $ref: '#/components/responses/403_ErrorResponse'
 *       404:
 *         $ref: '#/components/responses/404_ErrorResponse'
 *       500:
 *         $ref: '#/components/responses/500_ErrorResponse'
 */
router.delete(
  '/api/__version__/__resource-name-plural__/:_id',
  ...__resourceNameSingular__Controller__VERSION__.getMiddlewares(BasicControllerMethodsEnum.DELETE),
);

module.exports = router;
