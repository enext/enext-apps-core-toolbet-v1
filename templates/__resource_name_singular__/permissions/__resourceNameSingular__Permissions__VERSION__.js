/* eslint-disable camelcase */
// const RolesEnum = require('../../../app_core/components/roles/enums/RolesEnum');
const BasePermissions = require('web_core/permissions/BasePermissions');
const accessControlUtils = require('web_core/utils/accessControlUtils');
const BasicControllerMethodsEnum = require('web_core/enums/BasicControllerMethodsEnum');

const CREATE_ANY___RESOURCE_NAME_SINGULAR___ROLE = 'CREATE_ANY___RESOURCE_NAME_SINGULAR___ROLE';
const GET_MY___RESOURCE_NAME_SINGULAR___ROLE = 'GET_MY___RESOURCE_NAME_SINGULAR___ROLE';
const GET_ANY___RESOURCE_NAME_SINGULAR___ROLE = 'GET_ANY___RESOURCE_NAME_SINGULAR___ROLE';
const UPDATE_MY___RESOURCE_NAME_SINGULAR___ROLE = 'UPDATE_MY___RESOURCE_NAME_SINGULAR___ROLE';
const UPDATE_ANY___RESOURCE_NAME_SINGULAR___ROLE = 'UPDATE_ANY___RESOURCE_NAME_SINGULAR___ROLE';
const DELETE_MY___RESOURCE_NAME_SINGULAR___ROLE = 'DELETE_MY___RESOURCE_NAME_SINGULAR___ROLE';
const DELETE_ANY___RESOURCE_NAME_SINGULAR___ROLE = 'DELETE_ANY___RESOURCE_NAME_SINGULAR___ROLE';
const __RESOURCE_NAME_PLURAL___MASTER_ROLE = '__RESOURCE_NAME_PLURAL___MASTER_ROLE';

/**
 * Classe responsável por definir as permissões de acesso das rotas de __ResourceNameSingular__.
 *
 * @extends BasePermissions
 */
class __ResourceNameSingular__Permissions extends BasePermissions {
  /**
   * @constructor
   */
  constructor() {
    super();
    this.RESOURCE_NAME = '__RESOURCE_NAME_SINGULAR__';
  }

  /**
   * @override
   */
  getGrants() {
    return [
      {
        role: CREATE_ANY___RESOURCE_NAME_SINGULAR___ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.CREATE,
        attributes: ['*'],
      },
      {
        role: GET_MY___RESOURCE_NAME_SINGULAR___ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.GET],
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requester__ResourceNameSingular__Id: '$.requested__ResourceNameSingular__Id',
          },
        },
      },
      {
        role: GET_ANY___RESOURCE_NAME_SINGULAR___ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.GET, BasicControllerMethodsEnum.LIST],
        attributes: ['*'],
      },
      {
        role: UPDATE_MY___RESOURCE_NAME_SINGULAR___ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.UPDATE,
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requester__ResourceNameSingular__Id: '$.requested__ResourceNameSingular__Id',
          },
        },
      },
      {
        role: UPDATE_ANY___RESOURCE_NAME_SINGULAR___ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.UPDATE,
        attributes: ['*'],
      },
      {
        role: DELETE_MY___RESOURCE_NAME_SINGULAR___ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.DELETE,
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requester__ResourceNameSingular__Id: '$.requested__ResourceNameSingular__Id',
          },
        },
      },
      {
        role: DELETE_ANY___RESOURCE_NAME_SINGULAR___ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.DELETE],
        attributes: ['*'],
      },
      {
        role: __RESOURCE_NAME_PLURAL___MASTER_ROLE,
        resource: this.RESOURCE_NAME,
        action: [
          BasicControllerMethodsEnum.CREATE,
          BasicControllerMethodsEnum.GET,
          BasicControllerMethodsEnum.LIST,
          BasicControllerMethodsEnum.UPDATE,
          BasicControllerMethodsEnum.DELETE],
        attributes: ['*'],
      },
    ];
  }

  /**
   * @override
   */
  getExtends() {
    return [];
    // return [
    //   {
    //     role: RolesEnum.ENEXT_ROOT_ROLE,
    //     childrenRoles: [
    //       __RESOURCE_NAME_PLURAL___MASTER_ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.ENEXT_ADMIN_ROLE,
    //     childrenRoles: [
    //       CREATE_ANY___RESOURCE_NAME_SINGULAR___ROLE,
    //       UPDATE_ANY___RESOURCE_NAME_SINGULAR___ROLE,
    //       GET_ANY___RESOURCE_NAME_SINGULAR___ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.ENEXT_STAFF_ROLE,
    //     childrenRoles: [
    //       GET_ANY___RESOURCE_NAME_SINGULAR___ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.EXTERNAL_ORGANIZATION_ADMIN_ROLE,
    //     childrenRoles: [
    //       GET_MY___RESOURCE_NAME_SINGULAR___ROLE,
    //       UPDATE_MY___RESOURCE_NAME_SINGULAR___ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.EXTERNAL_ORGANIZATION_STAFF_ROLE,
    //     childrenRoles: [
    //       GET_MY___RESOURCE_NAME_SINGULAR___ROLE,
    //     ],
    //   },
    // ];
  }
}

const resourcePermissions = new __ResourceNameSingular__Permissions();

resourcePermissions.init(accessControlUtils);

module.exports = resourcePermissions;
