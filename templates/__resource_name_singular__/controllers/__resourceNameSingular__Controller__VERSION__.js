/* eslint-disable camelcase */
const ResourceBaseController = require('web_core/controllers/ResourceBaseController');
const resourceService = require('../services/__resourceNameSingular__Service__VERSION__');
const resourcePermissions = require('../permissions/__resourceNameSingular__Permissions__VERSION__');

/**
 * Classe de controller relacionada ao recurso __ResourceNameSingular__.
 *
 * @extends ResourceBaseController
 */
class __ResourceNameSingular__Controller__VERSION__ extends ResourceBaseController {
  /**
   * @constructor
   * @param {Object} params
   * @param {resourceService} params.resourceService
   * @param {resourcePermissions} params.resourcePermissions
   */
  constructor({
    resourceService,
    resourcePermissions,
  }) {
    super({
      resourceService,
      resourcePermissions,
    });

    this.permissionContext = () => (req) => {
      return {
        // requester__ResourceNameSingular__Id: req.user.organizationId,
        // requested__ResourceNameSingular__Id: req.params._id,
      };
    };
  }

  /**
   * @override
   */
  init() {
    super.init();
  }
}

const controllerInstance = new __ResourceNameSingular__Controller__VERSION__({
  resourceService,
  resourcePermissions,
});

controllerInstance.init();

module.exports = controllerInstance;
