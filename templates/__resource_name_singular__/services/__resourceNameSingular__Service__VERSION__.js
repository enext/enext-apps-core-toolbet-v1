/* eslint-disable camelcase */
const __ResourceNameSingular__Model = require('../models/__ResourceNameSingular__Model');
const BaseCrudService = require('web_core/services/BaseCrudService');
// const log = require('web_core/utils/logUtils');
// const nconf = require('nconf');

/**
 * Classe de serviço relacionada ao recurso __ResourceNameSingular__.
 * @extends BaseCrudService
 */
class __ResourceNameSingular__ServiceV1 extends BaseCrudService {}

const serviceInstance = new __ResourceNameSingular__ServiceV1({
  Model: __ResourceNameSingular__Model,
});

module.exports = serviceInstance;
