# Changelog

Todas as notáveis mudanças desse projeto serão documentadas nesse arquivo.

O formato é baseado em [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) e também é utilizado [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## TODO's

* Gerar testes de integração para os componentes criados.

## Unreleased

...

## Releases

## v1.0.0 - 2019-11-29

### Added

* Criação de um componente do tipo RESOURCE. Recurso com controller, modelo, serviço e rotas básicos.
* Criação de um componente do tipo ORGANIZATION_RESOURCE. Recurso com controller, modelo, serviço e rotas básicos.
* Geração dos arquivos de schema baseado no schema definido no mongoose. Foi utilizado um código próprio para geração dos schemas [generateDocsCommand.js](./src/commands/geneareteDocsCommand.js).
* Geração da documentação da Api utilizando swagger-jsdoc.
