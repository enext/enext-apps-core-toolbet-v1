# Enext Labs Apps Core Toolbet v1

Toolbet com comandos repetivos utilizados em projetos que utilizam as libs core do Enext Labs.

Esse projeto foi baseado no seguinte tutorial:

* [https://medium.com/netscape/a-guide-to-create-a-nodejs-command-line-package-c2166ad0452e](https://medium.com/netscape/a-guide-to-create-a-nodejs-command-line-package-c2166ad0452e). Tb disponível em [./docs/resources/web_pages_backup/guide_to_create_nodejs_clis](./docs/resources/web_pages_backup/guide_to_create_nodejs_clis).

A contrução do CLI é baseado na lib [https://github.com/yargs/yargs](https://github.com/yargs/yargs).

Devido a problemas com yarn, para esse projeto, exclusivamente, estamos utilizando o npm.

## Instalando o toolbet

```
npm install -g https://bitbucket.org/enext/enext-apps-core-toolbet-v1.git;
```

Não esqueça de alterar para versão que você deseja:

```
npm install -g https://bitbucket.org/enext/enext-apps-core-toolbet-v1.git#v1.4.3;
```

## Visualizando os comandos

```
eat --help;
```

## Npm link

Para visualizar as alterações de forma imediata, utilize o comando npm link:

```
npm link;
```

Ao finalizar as alterações locais, você deve voltar a dependência para a versão remota:

```
npm unlink;
```

Após o npm unlink será necessário fazer a instalação do toolbet novamente.

## Preparando um release

* Fazer as alterações no CHANGELOG.md
* Alterar a versão no package.json
* Criar o commit e fazer o push/merges/pull request para a master
* Criar uma tag com a versão
```
git tag -a vx.x.x;
```
* Fazer o push da tag
```
git push origin vx.x.x;
```

# TODO's, Releases & Unreleased

[CHANGELOG.md](./CHANGELOG.md)

# License

[LICENSE.md](./LICENSE.md)

