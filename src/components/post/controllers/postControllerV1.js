/* eslint-disable camelcase */
const ResourceBaseController = require('web_core/controllers/ResourceBaseController');
const resourceService = require('../services/postServiceV1');
const resourcePermissions = require('../permissions/postPermissionsV1');

/**
 * Classe de controller relacionada ao recurso Post.
 *
 * @extends ResourceBaseController
 */
class PostControllerV1 extends ResourceBaseController {
  /**
   * @constructor
   * @param {Object} params
   * @param {resourceService} params.resourceService
   * @param {resourcePermissions} params.resourcePermissions
   */
  constructor({
    resourceService,
    resourcePermissions,
  }) {
    super({
      resourceService,
      resourcePermissions,
    });
  }

  /**
   * @override
   */
  init() {
    super.init();
  }
}

const controllerInstance = new PostControllerV1({
  resourceService,
  resourcePermissions,
});

controllerInstance.init();

module.exports = controllerInstance;
