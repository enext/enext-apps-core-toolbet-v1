const express = require('express');
const router = new express.Router();
const BasicControllerMethodsEnum = require('web_core/enums/BasicControllerMethodsEnum');
// eslint-disable-next-line camelcase
const postControllerV1 = require('../controllers/postControllerV1');

/**
 * @swagger
 *
 * tags:
 *   - name: Post
 *     description: ''
 */

/**
 * @swagger
 *
 * /api/v1/posts:
 *   post:
 *     tags:
 *       - Post
 *     description: Cria um novo recurso.
 *     requestBody:
 *       content:
 *          'application/json':
 *             schema:
 *               $ref: '#/components/schemas/Post_createRequestSchema'
 *     responses:
 *       200:
 *         description: Criado com sucesso.
 *         content:
 *           'application/json':
 *             schema:
 *               $ref: '#/components/schemas/Post_getResponseSchema'
 *       400:
 *         $ref: '#/components/responses/400_ErrorResponse'
 *       401:
 *         $ref: '#/components/responses/401_ErrorResponse'
 *       403:
 *         $ref: '#/components/responses/403_ErrorResponse'
 *       500:
 *         $ref: '#/components/responses/500_ErrorResponse'
 */
router.post(
  '/api/v1/posts',
  ...postControllerV1.getMiddlewares(BasicControllerMethodsEnum.CREATE),
);

/**
 * @swagger
 *
 * /api/v1/posts/:_id:
 *   get:
 *     tags:
 *       - Post
 *     description: Obtém um novo recurso.
 *     parameters:
 *       - $ref: '#/components/parameters/_id'
 *       - $ref: '#/components/parameters/semanticId'
 *     responses:
 *       200:
 *         description: Recurso obtido com sucesso.
 *         content:
 *           'application/json':
 *             schema:
 *               $ref: '#/components/schemas/Post_getResponseSchema'
 *       400:
 *         $ref: '#/components/responses/400_ErrorResponse'
 *       401:
 *         $ref: '#/components/responses/401_ErrorResponse'
 *       403:
 *         $ref: '#/components/responses/403_ErrorResponse'
 *       404:
 *         $ref: '#/components/responses/404_ErrorResponse'
 *       500:
 *         $ref: '#/components/responses/500_ErrorResponse'
 */
router.get(
  '/api/v1/posts/:_id',
  ...postControllerV1.getMiddlewares(BasicControllerMethodsEnum.GET),
);

/**
 * @swagger
 *
 * /api/v1/posts:
 *   get:
 *     tags:
 *       - Post
 *     description: Lista os recursos. Retorno paginado.
 *     parameters:
 *       - $ref: '#/components/parameters/limit'
 *       - $ref: '#/components/parameters/page'
 *     responses:
 *       200:
 *         description: Página com todos os recursos obtidos.
 *         content:
 *           'application/json':
 *             schema:
 *               $ref: '#/components/schemas/Post_listResponseSchema'
 *       400:
 *         $ref: '#/components/responses/400_ErrorResponse'
 *       401:
 *         $ref: '#/components/responses/401_ErrorResponse'
 *       403:
 *         $ref: '#/components/responses/403_ErrorResponse'
 *       500:
 *         $ref: '#/components/responses/500_ErrorResponse'
 */
router.get(
  '/api/v1/posts',
  ...postControllerV1.getMiddlewares(BasicControllerMethodsEnum.LIST),
);

/**
 * @swagger
 *
 * /api/v1/posts/:_id:
 *   patch:
 *     tags:
 *       - Post
 *     description: Atualiza um recurso.
 *     parameters:
 *       - $ref: '#/components/parameters/_id'
 *       - $ref: '#/components/parameters/semanticId'
 *     requestBody:
 *       content:
 *          'application/json':
 *             schema:
 *               $ref: '#/components/schemas/Post_updateRequestSchema'
 *     responses:
 *       200:
 *         description: Atualizado com sucesso.
 *         content:
 *           'application/json':
 *             schema:
 *               $ref: '#/components/schemas/Post_getResponseSchema'
 *       400:
 *         $ref: '#/components/responses/400_ErrorResponse'
 *       401:
 *         $ref: '#/components/responses/401_ErrorResponse'
 *       403:
 *         $ref: '#/components/responses/403_ErrorResponse'
 *       404:
 *         $ref: '#/components/responses/404_ErrorResponse'
 *       500:
 *         $ref: '#/components/responses/500_ErrorResponse'
 */
router.patch(
  '/api/v1/posts/:_id',
  ...postControllerV1.getMiddlewares(BasicControllerMethodsEnum.UPDATE),
);

/**
 * @swagger
 *
 * /api/v1/posts/:_id:
 *   delete:
 *     tags:
 *       - Post
 *     description: Deleta o recurso.
 *     parameters:
 *       - $ref: '#/components/parameters/_id'
 *       - $ref: '#/components/parameters/semanticId'
 *     responses:
 *       204:
 *         description: Recurso removido com sucesso. Sem conteúdo.
 *       400:
 *         $ref: '#/components/responses/400_ErrorResponse'
 *       401:
 *         $ref: '#/components/responses/401_ErrorResponse'
 *       403:
 *         $ref: '#/components/responses/403_ErrorResponse'
 *       404:
 *         $ref: '#/components/responses/404_ErrorResponse'
 *       500:
 *         $ref: '#/components/responses/500_ErrorResponse'
 */
router.delete(
  '/api/v1/posts/:_id',
  ...postControllerV1.getMiddlewares(BasicControllerMethodsEnum.DELETE),
);

module.exports = router;
