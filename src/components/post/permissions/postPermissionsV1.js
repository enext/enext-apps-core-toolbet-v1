// const RolesEnum = require('app_core/components/roles/enums/RolesEnum');
const BasePermissions = require('web_core/permissions/BasePermissions');
const accessControlUtils = require('web_core/utils/accessControlUtils');
const BasicControllerMethodsEnum = require('web_core/enums/BasicControllerMethodsEnum');

const CREATE_ANY_POST_ROLE = 'CREATE_ANY_POST_ROLE';
const GET_MY_POST_ROLE = 'GET_MY_POST_ROLE';
const GET_ANY_POST_ROLE = 'GET_ANY_POST_ROLE';
const UPDATE_MY_POST_ROLE = 'UPDATE_MY_POST_ROLE';
const UPDATE_ANY_POST_ROLE = 'UPDATE_ANY_POST_ROLE';
const DELETE_MY_POST_ROLE = 'DELETE_MY_POST_ROLE';
const DELETE_ANY_POST_ROLE = 'DELETE_ANY_POST_ROLE';
const POSTS_MASTER_ROLE = 'POSTS_MASTER_ROLE';

/**
 * Classe responsável por definir as permissões de acesso das rotas de Organization.
 *
 * @extends BasePermissions
 */
class OrganizationPermissions extends BasePermissions {
  /**
   * @constructor
   */
  constructor() {
    super();
    this.RESOURCE_NAME = 'POST';
  }

  /**
   * @override
   */
  getGrants() {
    return [
      {
        role: CREATE_ANY_POST_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.CREATE,
        attributes: ['*'],
      },
      {
        role: GET_MY_POST_ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.GET],
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requesterOrganizationId: '$.requestedOrganizationId',
          },
        },
      },
      {
        role: GET_ANY_POST_ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.GET, BasicControllerMethodsEnum.LIST],
        attributes: ['*'],
      },
      {
        role: UPDATE_MY_POST_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.UPDATE,
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requesterOrganizationId: '$.requestedOrganizationId',
          },
        },
      },
      {
        role: UPDATE_ANY_POST_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.UPDATE,
        attributes: ['*'],
      },
      {
        role: DELETE_MY_POST_ROLE,
        resource: this.RESOURCE_NAME,
        action: BasicControllerMethodsEnum.DELETE,
        attributes: ['*'],
        condition: {
          Fn: 'EQUALS',
          args: {
            requesterOrganizationId: '$.requestedOrganizationId',
          },
        },
      },
      {
        role: DELETE_ANY_POST_ROLE,
        resource: this.RESOURCE_NAME,
        action: [BasicControllerMethodsEnum.DELETE],
        attributes: ['*'],
      },
      {
        role: POSTS_MASTER_ROLE,
        resource: this.RESOURCE_NAME,
        action: [
          BasicControllerMethodsEnum.CREATE,
          BasicControllerMethodsEnum.GET,
          BasicControllerMethodsEnum.LIST,
          BasicControllerMethodsEnum.UPDATE,
          BasicControllerMethodsEnum.DELETE],
        attributes: ['*'],
      },
    ];
  }

  /**
   * @override
   */
  getExtends() {
    // return [
    //   {
    //     role: RolesEnum.ENEXT_ROOT_ROLE,
    //     childrenRoles: [
    //       POSTS_MASTER_ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.ENEXT_ADMIN_ROLE,
    //     childrenRoles: [
    //       CREATE_ANY_POST_ROLE,
    //       UPDATE_ANY_POST_ROLE,
    //       GET_ANY_POST_ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.ENEXT_STAFF_ROLE,
    //     childrenRoles: [
    //       GET_ANY_POST_ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.EXTERNAL_ORGANIZATION_ADMIN_ROLE,
    //     childrenRoles: [
    //       GET_MY_POST_ROLE,
    //       UPDATE_MY_POST_ROLE,
    //     ],
    //   },
    //   {
    //     role: RolesEnum.EXTERNAL_ORGANIZATION_STAFF_ROLE,
    //     childrenRoles: [
    //       GET_MY_POST_ROLE,
    //     ],
    //   },
    // ];
  }
}

const resourcePermissions = new OrganizationPermissions();

resourcePermissions.init(accessControlUtils);

module.exports = resourcePermissions;
