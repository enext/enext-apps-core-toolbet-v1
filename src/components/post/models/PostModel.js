const mongoose = require('mongoose');
const modelUtils = require('web_core/utils/modelUtils');
require('mongoose-schema-jsonschema')(mongoose);
const semanticId = require('web_core/schema/types/semanticIdSchamaType');

// Definição do esquema
// eslint-disable-next-line camelcase
const PostSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    auto: true,
    description: 'Id do recurso.',
  },
  semanticId,
  name: {
    type: String,
    required: false,
    maxlength: 128,
    description: 'Nome do recurso.',
  },
  description: {
    type: String,
    required: false,
    maxlength: 1024,
    description: 'Descrição do recurso.',
  },
}, {
  timestamps: true,
});

// Índices
PostSchema.index({createdAt: 1});
PostSchema.index({createdAt: -1});
PostSchema.index({updatedAt: 1});
PostSchema.index({updatedAt: -1});
PostSchema.index({semanticId: 1}, {unique: true});

// Atributos protegidos no update
const updateProtectedAttributes = [
  'semanticId',
  'createdAt',
  'updatedAt',
];

// Atributos que podem ser utilizados no sort
const sortableAttributes = [
  'createdAt',
  'updatedAt',
];

module.exports = modelUtils.buildFromSchema({
  Schema: PostSchema,
  modelName: 'Post',
  updateProtectedAttributes,
  sortableAttributes,
});
