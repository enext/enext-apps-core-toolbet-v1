/* eslint-disable camelcase */
const PostModel = require('../models/PostModel');
const BaseCrudService = require('webcore/services/BaseCrudService');
// const log = require('webcore/utils/logUtils');
// const nconf = require('nconf');

/**
 * Classe de serviço relacionada ao recurso Post.
 * @extends BaseCrudService
 */
class PostServiceV1 extends BaseCrudService {}

const serviceInstance = new PostServiceV1({
  Model: PostModel,
});

module.exports = serviceInstance;
