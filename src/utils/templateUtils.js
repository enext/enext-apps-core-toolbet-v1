const fs = require('fs');
const path = require('path');

/**
 * Dicionário cuja chave representa a string a ser substituída
 * e o valor representa a string que a substituirá.
 * @typedef {Object} ReplaceMap
 */

/**
 * @private
 *
 * Função que faz o replace na string provida baseado no mapa provido
 *
 * @param {Object} params
 * @param {string} params.orginalString String original.
 * @param {ReplaceMap} params.replaceMap
 *
 * @return {string} String tratada, os replaces determinados por replaceMap já estão feitos.
 */
function replace({
  orginalString,
  replaceMap,
}) {
  console.info(`replace chamado`);
  console.info(`orginalString.slice(0, 100): ${orginalString.slice(0, 100)}`);
  let treatedString = orginalString.slice(0);
  Object.keys(replaceMap).map((toBeReplaced) => {
    // TODO: Testar se todas ocorrências são substituídas.
    treatedString = treatedString.replace(new RegExp(toBeReplaced, 'g'), replaceMap[toBeReplaced]);
  });
  console.info(`treatedString.slice(0, 100): ${treatedString.slice(0, 100)}`);
  return treatedString;
}

/**
 * Função que gera os novos diretórios e arquivos baseado nos templates.
 *
 * @param {Object} params
 * @param {string} params.templateDirectoryFullPath Diretório com subdiretórios e arquivos do template
 * @param {string} params.parentOfOutputDirectoryFullPath Diretório que receberá, como subdiretório, o diretório baseado
 * no template após os devidos replaces.
 * @param {ReplaceMap} params.replaceMap
 */
function generateDirsAndFilesBasedOnTemplate({
  templateDirectoryFullPath,
  parentOfOutputDirectoryFullPath,
  replaceMap,
}) {
  console.info(`templateDirectoryFullPath: ${templateDirectoryFullPath}`);

  templateDirectoryName = templateDirectoryFullPath.split(path.sep).pop();
  console.info(`templateDirectoryName: ${templateDirectoryName}`);

  // Criando o novo diretório
  const outputDirectoryName = replace({
    orginalString: templateDirectoryName,
    replaceMap,
  });

  console.log(`outputDirectoryName: ${outputDirectoryName}`);

  const outputDirectory = path.join(parentOfOutputDirectoryFullPath, outputDirectoryName);

  console.info(`outputDirectory: ${outputDirectory}`);

  if (!fs.existsSync(outputDirectory)) {
    fs.mkdirSync(outputDirectory, {recursive: true});
  } else {
    throw new Error(`Directory ${outputDirectory} already exists. It must be empty to generate the template files.`);
  }

  // Iterando por todos os subdiretórios e arquivos e fazendo o replace
  fs.readdirSync(templateDirectoryFullPath).map((dirOrFileName) => {
    console.info(`dirOrFileName: ${dirOrFileName}`);
    const templateFileOrDirFullPath = path.join(templateDirectoryFullPath, dirOrFileName);
    const isDirectory = fs.lstatSync(templateFileOrDirFullPath).isDirectory();

    if (isDirectory) {
      console.info(`isDirectory`);
      generateDirsAndFilesBasedOnTemplate({
        templateDirectoryFullPath: templateFileOrDirFullPath,
        parentOfOutputDirectoryFullPath: outputDirectory,
        replaceMap,
      });
    } else {
      const filename = dirOrFileName;
      const treatedFilename = replace({
        orginalString: filename,
        replaceMap,
      });
      console.info(`treatedFilename: ${treatedFilename}`);
      const originalFileFullPath = path.join(templateDirectoryFullPath, filename);
      const originalFileContent = fs.readFileSync(originalFileFullPath).toString();
      const treatedContent = replace({
        orginalString: originalFileContent,
        replaceMap,
      });
      fs.writeFileSync(path.join(parentOfOutputDirectoryFullPath, outputDirectoryName, treatedFilename), treatedContent);
    }
  });
}

module.exports = {
  generateDirsAndFilesBasedOnTemplate,
};
