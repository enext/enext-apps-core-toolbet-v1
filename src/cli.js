#!/usr/bin/env node

const yargs = require('yargs');

const yargsDefinition = yargs.usage('Usage: $0 <command> [options]');
require('./commands/generateDocsCommand').register(yargsDefinition);
require('./commands/generate_new_component_command/generateNewComponentCommand').register(yargsDefinition);

yargsDefinition
  // boilerplate
  .strict()
  .help('h')
  .alias('h', 'help')
  .version(require('../package').version)
  .argv;
