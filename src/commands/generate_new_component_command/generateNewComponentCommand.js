/* eslint-disable camelcase */
const NewComponentCommandTypesEnum = require('./NewComponentCommandTypesEnum');
const ComponentNameStringTypeEnum = require('./ComponentNameStringTypeEnum');
const templateUtils = require('../../utils/templateUtils');
const BaseCommand = require('../BaseCommand');
const path = require('path');
const nconf = require('nconf');

const templatesDir = path.join(__dirname, '../../../templates');

/**
 * Classe responsável por lidar com o comando para gerar um novo componente
 * baseado em templates pré-estabelecidos.
 */
class GenerateNewComponentCommand extends BaseCommand {
  /**
   *
   * @param {Object} params
   * @param {string} params.__component_name__ String com caracteres minusculos separados por '_' representando o nome do component (plural ou singular)
   * @param {ComponentNameStringTypeEnum} params.type
   *
   * @return {string} String com o tipo correto.
   */
  convertToOtherStringType({
    __component_name__,
    type,
  }) {
    switch (type) {
      case ComponentNameStringTypeEnum.CAMEL_CASE_FIRST_UPPER:
        return __component_name__
          .split('_')
          .filter((c) => c != '')
          .map((w) => w.charAt(0).toUpperCase() + w.slice(1))
          .join('');
      case ComponentNameStringTypeEnum.CAMEL_CASE_FIRST_LOWER:
        return __component_name__
          .split('_')
          .filter((c) => c != '')
          .map((w, idx) => {
            if (idx === 0) {
              return w.charAt(0).toLowerCase() + w.slice(1);
            } else {
              return w.charAt(0).toUpperCase() + w.slice(1);
            }
          })
          .join('');
      case ComponentNameStringTypeEnum.CAPS_SEPARATED_BY_UNDERLINE:
        return __component_name__
          .split('_')
          .filter((c) => c != '')
          .map((w) => w.toUpperCase())
          .join('_');
      case ComponentNameStringTypeEnum.LOWERCASE_SEPARATED_BY_HIFEN:
        return __component_name__
          .split('_')
          .filter((c) => c != '')
          .join('-');
      default:
        throw new Error('Invalid value for ComponentNameStringTypeEnum');
    }
  }

  /**
   * Função responsável pela geração de um novo recurso baseado nos repectivos templates.
   */
  generateResource({
    version,
    __component_name_singular__,
    __component_name_plural__,
    outputDirectory,
  }) {
    const replaceMap = {
      '__resource_name_singular__': __component_name_singular__,
      '__ResourceNameSingular__': this.convertToOtherStringType({
        __component_name__: __component_name_singular__,
        type: ComponentNameStringTypeEnum.CAMEL_CASE_FIRST_UPPER,
      }),
      '__RESOURCE_NAME_SINGULAR__': this.convertToOtherStringType({
        __component_name__: __component_name_singular__,
        type: ComponentNameStringTypeEnum.CAPS_SEPARATED_BY_UNDERLINE,
      }),
      '__RESOURCE_NAME_PLURAL__': this.convertToOtherStringType({
        __component_name__: __component_name_plural__,
        type: ComponentNameStringTypeEnum.CAPS_SEPARATED_BY_UNDERLINE,
      }),
      '__resourceNameSingular__': this.convertToOtherStringType({
        __component_name__: __component_name_singular__,
        type: ComponentNameStringTypeEnum.CAMEL_CASE_FIRST_LOWER,
      }),
      '__resource-name-plural__': this.convertToOtherStringType({
        __component_name__: __component_name_plural__,
        type: ComponentNameStringTypeEnum.LOWERCASE_SEPARATED_BY_HIFEN,
      }),
      '__version__': version,
      '__VERSION__': version.toUpperCase(),
    };

    console.info(`replaceMap: ${JSON.stringify(replaceMap, null, 2)}`);

    templateUtils.generateDirsAndFilesBasedOnTemplate({
      templateDirectoryFullPath: path.resolve(path.join(templatesDir, '__resource_name_singular__')),
      parentOfOutputDirectoryFullPath: outputDirectory,
      replaceMap,
    });
  }

  /**
   * Função responsável pela geração de um novo recurso baseado nos repectivos templates.
   */
  generateOrganizationResource({
    version,
    __component_name_singular__,
    __component_name_plural__,
    outputDirectory,
  }) {
    const replaceMap = {
      '__resource_name_singular__': __component_name_singular__,
      '__organization_resource_name_singular__': __component_name_singular__,
      '__ResourceNameSingular__': this.convertToOtherStringType({
        __component_name__: __component_name_singular__,
        type: ComponentNameStringTypeEnum.CAMEL_CASE_FIRST_UPPER,
      }),
      '__RESOURCE_NAME_SINGULAR__': this.convertToOtherStringType({
        __component_name__: __component_name_singular__,
        type: ComponentNameStringTypeEnum.CAPS_SEPARATED_BY_UNDERLINE,
      }),
      '__RESOURCE_NAME_PLURAL__': this.convertToOtherStringType({
        __component_name__: __component_name_plural__,
        type: ComponentNameStringTypeEnum.CAPS_SEPARATED_BY_UNDERLINE,
      }),
      '__resourceNameSingular__': this.convertToOtherStringType({
        __component_name__: __component_name_singular__,
        type: ComponentNameStringTypeEnum.CAMEL_CASE_FIRST_LOWER,
      }),
      '__resource-name-plural__': this.convertToOtherStringType({
        __component_name__: __component_name_plural__,
        type: ComponentNameStringTypeEnum.LOWERCASE_SEPARATED_BY_HIFEN,
      }),
      '__version__': version,
      '__VERSION__': version.toUpperCase(),
    };

    console.info(`replaceMap: ${JSON.stringify(replaceMap, null, 2)}`);

    templateUtils.generateDirsAndFilesBasedOnTemplate({
      templateDirectoryFullPath: path.resolve(path.join(templatesDir, '__organization_resource_name_singular__')),
      parentOfOutputDirectoryFullPath: outputDirectory,
      replaceMap,
    });
  }

  /**
   * @override
   */
  run(argv) {
    try {
      nconf.env().defaults({store: argv});

      console.info(`---`);
      console.info('Running GenerateNewComponentCommand.run...');

      // Obtendo os valores definidos pelo usuário
      const type = nconf.get('type');
      const apiVersion = nconf.get('apiVersion');
      const __component_name_singular__ = nconf.get('__component_name_singular__');
      const __component_name_plural__ = nconf.get('__component_name_plural__');
      const outputDirectory = nconf.get('outputDirectory');

      // Imprimindo os valores definidos pelo usuário
      console.info(`type: ${type}`);
      console.info(`apiVersion: ${apiVersion}`);
      console.info(`__component_name_singular__: ${__component_name_singular__}`);
      console.info(`__component_name_plural__: ${__component_name_plural__}`);
      console.info(`outputDirectory: ${outputDirectory}`);

      // Empacotando os valores em um novo objeto para facilitar o envio para cada uma das funções
      const config = {
        version: `v${apiVersion}`,
        __component_name_singular__,
        __component_name_plural__,
        outputDirectory,
      };

      // Chamando a respectiva função dependendo do tipo enviado
      switch (type) {
        case NewComponentCommandTypesEnum.RESOURCE:
          this.generateResource(config);
          break;
        case NewComponentCommandTypesEnum.ORGANIZATION_RESOURCE:
          this.generateOrganizationResource(config);
          break;
      }

      console.info(`---`);
      console.info('SUCCESS');
    } catch (err) {
      console.error('FAILED');
      if (err) {
        console.error(err);
      }
      process.exit(1);
    }
  }

  /**
   * @override
   */
  register(yargs) {
    yargs
      .command(
        'generate:component',
        'Gera um componente baseado nos templates pré-cadastrados.',
        (yargs) => {
          return yargs
            .option('type', {
              nargs: 1,
              describe: 'Tipo de componente a ser gerado',
              demand: true,
              choices: [
                ...NewComponentCommandTypesEnum.getValues(),
              ],
            })
            .option('apiVersion', {
              nargs: 1,
              describe: 'Versão da API. Ex.: 1.',
              demand: false,
              default: 1,
            })
            .option('__component_name_singular__', {
              nargs: 1,
              describe: 'Nome do componente no singular, com letras minusculas e separado por \'_\'',
              demand: true,
            })
            .option('__component_name_plural__', {
              nargs: 1,
              describe: 'Nome do componente no plural, com letras minusculas e separado por \'_\'',
              demand: true,
            })
            .option('outputDirectory', {
              nargs: 1,
              describe: 'Diretório no qual a pasta principal desse componente será criado',
              demand: false,
              default: 'src/components',
            });
        },
        (argv) => this.run(argv));
  }
}

const commandInstance = new GenerateNewComponentCommand();

module.exports = commandInstance;
