const RESOURCE = 'RESOURCE';
const ORGANIZATION_RESOURCE = 'ORGANIZATION_RESOURCE';

module.exports = {
  RESOURCE,
  ORGANIZATION_RESOURCE,
  getValues: require('../../utils/enumGetValuesUtil'),
};
