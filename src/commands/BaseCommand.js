/**
 * Classe base de um comando.
 */
class BaseCommand {
  /**
   * @abstract
   *
   * Função pricipal que executa todas as chamadas auxiliares do comando.
   */
  run() {
    throw new Error(`Must be implemented`);
  }

  /**
   * @abstract
   *
   * Função que registra o comando em questão no yargs.
   *
   * @param {Object} yargs Yargs command.
   */
  register() {
    throw new Error(`Must be implemented`);
  }
}

module.exports = BaseCommand;
