const fs = require('fs');
const path = require('path');
const nconf = require('nconf');
const glob = require('glob');
const mongoose = require('mongoose');
// eslint-disable-next-line no-unused-vars
const {spawnSync, SpawnSyncReturns} = require('child_process'); // SpawnSyncReturns é utilizado pelo jsdoc
const yaml = require('yaml');
const BaseCommand = require('./BaseCommand');

require('mongoose-schema-jsonschema')(mongoose);

const apidocSwaggerExec = path.join(__dirname, '..', '..', 'node_modules', '.bin', 'swagger-jsdoc');
const jsDocExec = path.join(__dirname, '..', '..', 'node_modules', '.bin', 'jsdoc');

/**
 * Classe responsável por gerar pelo comando de geração da documentação.
 */
class GenerateDocsCommand extends BaseCommand {
  /**
   * Método que gera o yaml a partir do json
   *
   * @param {Object} params
   * @param {Object} params.jsonSchema
   * @param {string} params.definitionName Nome do schema swagger do model em questão.
   *
   * @return {string} String representando o conteúdo yaml da documentação swagger envolto por comentários jsdoc.
   */
  getJsDocYaml({
    jsonSchema,
    definitionName,
  }) {
    const yamlString = yaml.stringify(jsonSchema);

    let s = '/**\n';
    s += ' * @swagger\n';
    s += ' *\n';
    s += ' * components:\n';
    s += ' *   schemas:\n';
    s += ` *     ${definitionName}:\n`;
    yamlString.split('\n').map((line) => {
      if (line) {
        s += ` *       ${line}\n`;
      }
    });
    s = s.slice(0, s.length - 1);
    s += '\n */\n';

    return s;
  }

  // TODO: Detalhar os parâmetros recebidos.
  /**
   * Função que escreve os arquivos js (comentário swagger-jsdoc) e json (json schema) a partir do jsonShema enviado
   *
   * @param {Object} params
   */
  writeSchema({
    jsonSchema,
    outputDirectory,
    schemaName,
    model,
  }) {
    const jsonDir = path.join(outputDirectory, 'json');
    const jsDir = path.join(outputDirectory, 'js');

    if (!fs.existsSync(jsonDir)) {
      fs.mkdirSync(jsonDir, {recursive: true});
    }
    if (!fs.existsSync(jsDir)) {
      fs.mkdirSync(jsDir, {recursive: true});
    }

    fs.writeFileSync(path.join(jsonDir, `${schemaName}.json`), JSON.stringify(jsonSchema, null, 2));
    fs.writeFileSync(path.join(jsDir, `${schemaName}.js`), this.getJsDocYaml({
      jsonSchema,
      definitionName: `${schemaName}`,
    }));
  }

  // TODO: Detalhar params
  /**
   * Função que obtém o jsonSchema original gerado pela lib mongoose-schema-jsonschema e faz as devidas alterações:
   *   - Remoção do _id e semanticId do esquema de criação;
   *   - Remoção da tag optional de createdAt, updatedAt e __v;
   *   - Adaptações para gerar o esquema da página;
   *   - Remoção dos atributos protegidos no esquema de update.
   *
   * @param {Object} params
   */
  writeSchemas({jsonSchema, model, outputDirectory, updateProtectedAttributes}) {
    if ('createdAt' in jsonSchema.properties) {
      jsonSchema.properties.createdAt['description'] = 'Tempo de criação do documento. Este valor é auto-gerado.';
      if (jsonSchema.required) {
        jsonSchema.required = [...jsonSchema.required, 'createdAt'];
      }
    }
    if ('updatedAt' in jsonSchema.properties) {
      jsonSchema.properties.updatedAt['description'] = 'Tempo da última atualização do documento. Este valor é auto-gerado.';
      if (jsonSchema.required) {
        jsonSchema.required = [...jsonSchema.required, 'updatedAt'];
      }
    }
    if ('__v' in jsonSchema.properties) {
      jsonSchema.properties.__v['description'] = 'Mongoose version. Incrementado em toda alteração do modelo/schema.';
      if (jsonSchema.required) {
        jsonSchema.required = [...jsonSchema.required, '__v'];
      }
    }

    this.writeSchema({
      jsonSchema: {...jsonSchema, title: `${model}_getResponseSchema`},
      outputDirectory,
      schemaName: `${model}_getResponseSchema`,
      model,
      updateProtectedAttributes,
    });

    const pageJsonSchema = {
      type: 'object',
      properties: {
        docs: {
          type: 'array',
          items: {
            ...jsonSchema,
            description: model + '.',
          },
          description: 'Lista de documentos do tipo ',
        },
        totalDocs: {
          type: 'number',
          description: 'Número total de documentos para a query em questão.',
        },
        limit: {
          type: 'number',
          description: 'Número de documentos por página.',
        },
        hasPrevPage: {
          type: 'boolean',
          description: 'Indica se existe uma página anterior.',
        },
        hasNextPage: {
          type: 'boolean',
          description: 'Indica se existe uma próxima página.',
        },
        page: {
          type: 'number',
          description: 'Página atual.',
        },
        totalPages: {
          type: 'number',
          description: 'Número total de páginas.',
        },
        pagingCounter: {
          type: 'number',
          description: 'Posição geral do primeiro documento dessa página.',
        },
        prevPage: {
          type: 'number',
          description: 'Número da página anterior.',
        },
        nextPage: {
          type: 'number',
          description: 'Número da próxima página.',
        },
      },
      required: [
        'docs',
        'totalDocs',
        'limit',
        'hasPrevPage',
        'hasNextPage',
        'page',
        'totalPages',
        'pagingCounter',
        'prevPage',
        'nextPage',
      ],
    };

    this.writeSchema({
      jsonSchema: {...pageJsonSchema, title: `${model}_listResponseSchema`},
      outputDirectory,
      schemaName: `${model}_listResponseSchema`,
      model,
    });

    if ('_id' in jsonSchema.properties) {
      delete jsonSchema.properties['_id'];
    }
    if ('createdAt' in jsonSchema.properties) {
      delete jsonSchema.properties['createdAt'];
    }
    if ('updatedAt' in jsonSchema.properties) {
      delete jsonSchema.properties['updatedAt'];
    }
    if ('__v' in jsonSchema.properties) {
      delete jsonSchema.properties['__v'];
    }

    this.writeSchema({
      jsonSchema: {...jsonSchema, title: `${model}_createRequestSchema`},
      outputDirectory,
      schemaName: `${model}_createRequestSchema`,
      model,
    });

    updateProtectedAttributes.map((attrKey) => {
      if (attrKey in jsonSchema.properties) {
        delete jsonSchema.properties[attrKey];
      }
    });

    this.writeSchema({
      jsonSchema: {...jsonSchema, title: `${model}_updateRequestSchema`},
      outputDirectory,
      schemaName: `${model}_updateRequestSchema`,
      model,
    });
  }

  // TODO: Detalhar params
  // TODO: Implementar e testar
  /**
   * Função que gera o paramter sort para documentar os endpoints
   * de list baseado no valor definido em Model.getSortableAttributes().
   *
   * @param {Object} params
   */
  writeListSort({sortableAttributes, model, outputDirectory}) {
    let stringOfSortableAttributes = ``;
    for (let i = 0; i < sortableAttributes.length; i++) {
      stringOfSortableAttributes += `'${sortableAttributes[i]}','+${sortableAttributes[i]}','-${sortableAttributes[i]}'`;
      if (i < sortableAttributes.length - 1) {
        stringOfSortableAttributes += `,`;
      }
    }

    let s = ``;
    s += `/**\n`;
    s += `* @apiDefine ${model}ListSortQueryParameter\n`;
    s += `* @apiParam (Query Parameters) {String=${stringOfSortableAttributes}} sort
 Parâmetro que indica a ordenação retornada.\n`;
    s += '*/\n';
    fs.writeFileSync(path.join(outputDirectory, `${model}ListSortQueryParameter.js`), s);
  }

  /**
   * Função que gera os arquivos de esquema a partir dos models passados.
   */
  generateSchemaFiles() {
    console.info(`---`);
    console.info('Runnig generateSchemaFiles...');

    const inputGlobOfSchemas = nconf.get('inputGlobOfSchemas');
    const outputDirectoryOfSchemas = nconf.get('outputDirectoryOfSchemas');

    console.info(`inputGlobOfSchemas: ${inputGlobOfSchemas}`);
    console.info(`outputDirectoryOfSchemas: ${outputDirectoryOfSchemas}`);

    const globString = path.join(process.cwd(), inputGlobOfSchemas);
    console.info(`globString: ${globString}`);

    const dirs = glob.sync(globString);
    console.info(`dirs: ${dirs}`);

    dirs.map((dir) => {
      console.info(`dir: ${dir}`);

      const Model = require(dir);
      const jsonSchema = Model.jsonSchema();

      this.writeSchemas({
        jsonSchema,
        model: Model.modelName,
        updateProtectedAttributes: Model.getUpdateProtectedAttributes(),
        outputDirectory: outputDirectoryOfSchemas,
      });

      this.writeListSort({
        sortableAttributes: Model.getSortableAttributes(),
        model: Model.modelName,
        outputDirectory: outputDirectoryOfSchemas,
      });
    });
  }

  /**
   * Trata a resposta de uma spawnSync. Caso hajam erros o processo é encerrado.
   *
   * @param {SpawnSyncReturns} spawnResult
   */
  treatSpawnResult(spawnResult) {
    if (spawnResult.stdout) {
      console.info(spawnResult.stdout.toString());
    }
    if (spawnResult.status !== 0) {
      if (spawnResult.stderr && spawnResult.stderr.toString()) {
        console.error(spawnResult.stderr.toString());
      }
      throw spawnResult.error;
    }
  }

  /**
   * Função responsável por gerar o arquivo swagger.
   */
  generateSwaggerFile() {
    console.info(`---`);
    console.info(`Generating swagger file...`);

    const outputFileDestinesOfSwaggerJson = nconf.get('outputFileDestinesOfSwaggerJson');

    const inputGlobsOfSwaggerDocFiles = nconf.get('inputGlobsOfSwaggerDocFiles');

    const nodePackage = require(path.join(process.cwd(), 'package'));
    if (nodePackage.swaggerJsDocConfig) {
      let swaggerOptions = {
        info: {
          title: nodePackage.name,
          version: nodePackage.version,
          description: nodePackage.description,
        },
      };
      swaggerOptions = {...swaggerOptions, ...nodePackage.swaggerJsDocConfig};
      if (nconf.get('host')) {
        swaggerOptions.host = nconf.get('host');
      }
      if (nconf.get('basePath')) {
        swaggerOptions.basePath = nconf.get('basePath');
      }
      swaggerOptions.apis = inputGlobsOfSwaggerDocFiles.toString().trim().split(',');

      const tempSwaggerOptionsFilename = 'temp_swagger_options.json';

      fs.writeFileSync(tempSwaggerOptionsFilename, JSON.stringify(swaggerOptions, null, 2));

      outputFileDestinesOfSwaggerJson.toString().trim().split(',').map((outFile) => {
        const dir = path.dirname(outFile);

        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir, {recursive: true});
        }

        const spawnResult = spawnSync(`${apidocSwaggerExec}`, [
          '-d', `${tempSwaggerOptionsFilename}`,
          '-o', `${outFile}`,
        ]);
        this.treatSpawnResult(spawnResult);
      });

      fs.unlinkSync(tempSwaggerOptionsFilename);
    } else {
      console.info(`Not generating swagger file`);
      console.info(`If you want generate the swagger file, you must add 'swaggerJsDocConfig' to your package.json`);
    }
  }

  /**
   * Função responsável por gerar os arquivos da documentação js.
   */
  generateJsDocFiles() {
    console.info(`---`);
    console.info(`Generating JSDoc files...`);

    const inputDirectoriesOfJsDoc = nconf.get('inputDirectoriesOfJsDoc');
    const readmeFileForJsDoc = nconf.get('readmeFileForJsDoc');
    const outputDirectoryOfJsDocFiles = nconf.get('outputDirectoryOfJsDocFiles');

    console.info(`inputDirectoriesOfJsDoc: ${inputDirectoriesOfJsDoc}`);
    console.info(`readmeFileForJsDoc: ${readmeFileForJsDoc}`);
    console.info(`outputDirectoryOfJsDocFiles: ${outputDirectoryOfJsDocFiles}`);

    const spawnResult = spawnSync(`${jsDocExec}`, [
      '-r',
      '-d', `${outputDirectoryOfJsDocFiles}`,
      ...inputDirectoriesOfJsDoc.toString().split(','),
    ]);
    this.treatSpawnResult(spawnResult);
  }

  /**
   * @override
   *
   * Função pricipal que executa todas as chamadas auxiliares do comando generateDocsCommand.
   *
   * @param {Object} argv Argumentos de programa https://yargs.js.org/docs/#api-argv.
   */
  run(argv) {
    try {
      nconf.env().defaults({store: argv});

      console.info(`---`);
      console.info('Running generateDocsCommand.run...');

      this.generateSchemaFiles();
      this.generateSwaggerFile();
      this.generateJsDocFiles();

      console.info(`---`);
      console.info('SUCCESS');
    } catch (err) {
      console.error('FAILED');
      if (err) {
        console.error(err);
      }
      process.exit(1);
    }
  }

  /**
   * @override
   *
   * Função que registra o comando em questão no yargs.
   *
   * @param {Object} yargs Yargs command.
   */
  register(yargs) {
    yargs
      .command(
        'generate:docs',
        'Gera a documentação da API baseada em https://www.npmjs.com/package/swagger-jsdoc e tb dos arquivos js baseado em https://devdocs.io/jsdoc/.',
        (yargs) => {
          return yargs
            .option('inputGlobOfSchemas', {
              nargs: 1,
              describe: 'Glob contendo os arquivos input para geração da documentação. Ex.: "src/**/*Model.js".',
              demand: false,
              default: 'src/**/*Model.js',
            })
            // ---
            .option('inputDirectoriesOfJsDoc', {
              describe: 'Diretórios de entrada com os comentários JSDoc. Ex.: "src".',
              demand: false,
              default: ['src'],
            })
            // ---
            .option('readmeFileForJsDoc', {
              describe: 'Local do README.md utilzado pelo JSDoc.',
              demand: false,
              default: 'README.md',
            })
            // ---
            .option('inputGlobsOfSwaggerDocFiles', {
              describe: 'Globs especifcando on estão os arquivos com os comentários de https://www.npmjs.com/package/swagger-jsdoc.',
              demand: false,
              default: ['./src/**/*.js', 'docs/api/schemas/js/*.js', 'docs/api/errors.js', 'docs/api/parameters.js'],
            })
            // ---
            .option('outputDirectoryOfSchemas', {
              nargs: 1,
              describe: 'Diretório de saída dos arquivos de schema. Ex.: "docs/api/schema"',
              demand: false,
              default: 'docs/api/schemas',
            })
            // ---
            .option('outputFileDestinesOfSwaggerJson', {
              describe: 'Diretórios de saída do swagger.json gerado por https://www.npmjs.com/package/swagger-jsdoc',
              demand: false,
              default: ['docs/api/swagger.json'],
            })
            // ---
            .option('outputDirectoryOfJsDocFiles', {
              describe: 'Diretório de saída com os arquivos de documentação js gerado por https://devdocs.io/jsdoc/',
              demand: false,
              default: 'docs/js',
            })
            // ---
            .option('host', {
              describe: 'Host da API. Usado pelo Swagger.',
              demand: false,
            })
            // ---
            .option('basePath', {
              describe: 'Base path da API. Usado pelo Swagger.',
              demand: false,
            });
        },
        (argv) => this.run(argv));
  }
}

const commandInstance = new GenerateDocsCommand();

module.exports = commandInstance;
